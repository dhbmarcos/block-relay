"""Data Repository

Definitions for Repository file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/repository.py)

## Examples

### Create New Repository In Default Location

To create new repository in default location:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME")
~~~

### Create New Repository In Custom Location

To create new repository in custom location:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME", "MY_CUSTOM_LOCATION")
~~~

### To Get Path Of Repository Location

Cast `Repository` to `str`:

~~~python
from viewrelay import repository

repository = repository.Repository("NAME")
path_of_repository = str(repository)
~~~

### To Change Default Repository Location

Change `REPOSITORY_LOCATION`:

~~~python
from viewrelay import repository

repository.REPOSITORY_LOCATION = "MY_DEFAULT_REPOSITORY"

repository = repository.Repository("NAME")
~~~

> :warning: **WARNING**
>
> Changes in the `REPOSITORY_LOCATION` after create an Agent, File, Repository or View object, may cause unexpected behavior and data loss.

### To Synchronize A Repository In One Way.

Use `sync` method:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME")
result = repository.sync("SOURCE-REPOSITORY-PATH")

print(result["success"])
print(result["failure"])
~~~

Reference
=========
"""

import os
import shutil


REPOSITORY_LOCATION = "tmp"
"""`str`: Default repository location."""


class Repository():
    """Data repository."""

    def __init__(self, name: str, location: str = REPOSITORY_LOCATION):
        """Create new repository in default repository location.
        
        If not location input, then value of `REPOSITORY_LOCATION` will be used.
        If directory already exist do nothing.
        The directory will be created at `location`/`name`.

        :param name:     Name of directory to be created in default repository location.
        :type  name:     str
        :param location: Optional custom repository location. Default to `REPOSITORY_LOCATION`.
        :type  location: str

        :raise Exception: Failure to create repository.
        """
        try:
            self._path = f"{location}/{name}" 
            os.makedirs(self._path, exist_ok=True)
        except Exception as error:
            raise type(error)(f"Failure to create repository: {str(error)}")


    def sync(self, source: str) -> list:
        """ Synchronize two repositories in one way.

        The synchronization is from source to this repository.
        On error on copy file from source, the information error will be set in return, in failure list.  

        :param source: Path of source repository.
        :type  source: str

        :raise Exception: Failure in source repositories.

        :return: result
            - `list` `str`  **"success"**: id of file copied.
            - `list` `dict` **"failure"**:
                - `str` **"id"**    : id of file not copied.
                - `str` **"error"** : error information about file copy.
        :rtype: dict
        """
        result = {
            "success": [],
            "failure": []
        }

        for id in os.listdir(source):
            if not os.path.exists(f"{self._path}/{id}"):
                if not os.path.isfile(f"{source}/{id}"):
                    result["failure"].append({"id": id, "error": "Is directory."})
                    continue
                try:
                    shutil.copy(f"{source}/{id}", f"{self._path}/{id}")
                    result["success"].append(id)
                except Exception as error:
                    result["failure"].append({"id": id, "error": str(error)})

        return result


    def __str__(self):
        """Get path of repository."""
        return self._path
