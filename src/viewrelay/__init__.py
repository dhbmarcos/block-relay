"""
Initialization of View Relay module.

## Example

~~~python
import viewrelay
~~~

Reference
=========
"""


def __get_checksum__() -> str:
    """Calculate hash of source code.

    :return: SHA3-224, encoded in base58, and then, encoded in UTF-8.
    :rtype: str
    """
    import os
    import hashlib
    import base58

    checksum = b""
    for path, directories, files in os.walk(os.path.dirname(__file__)):
        for file in files:
            name, extension = os.path.splitext(file)
            if extension != ".py":
                continue

            with open(f"{path}/{file}", "rb") as file:
                checksum = hashlib.sha3_224(checksum + file.read()).digest()

    checksum = base58.b58encode(checksum)
    checksum = str(checksum, "utf-8")
    return checksum


__version__   = "0.3.0"
"""
Version of module (SIMVER)
See <https://simver.org> for more information.
"""

__copyright__ = "Copyright (C) 2024 D. H. B. Marcos"
"""Copyright credits of module"""

__release__   = "public"
"""Product release code"""

__readiness__ = "undefined"
"""
Simplest Technology Readiness Level.
See <https://dhbmarcos.gitlab.io/strl> for more information.
"""

__build__     = "24096.20635"
"""
Product build instant code.

Specification of build value:
- First five digits:
    - digits 1 and 2: year of build;
    - digits 3 and 4: week number of build;
    - digit  5:       day of week of build;
- separator (`.`);
- Second from start of day;
"""

__checksum__  = __get_checksum__()
"""Hash SHA3-224 of source code, generated in run time."""


from viewrelay.agent      import Agent
from viewrelay.data       import Data
from viewrelay.encryption import Asymmetric
from viewrelay.encryption import Symmetric
from viewrelay.file       import File
from viewrelay.repository import Repository
from viewrelay.view       import View
