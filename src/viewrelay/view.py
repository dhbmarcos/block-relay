import os
import json


from viewrelay.repository import Repository
from viewrelay.file       import File
from viewrelay.encryption import Symmetric
from viewrelay.encryption import Asymmetric
from viewrelay.agent      import Agent
from viewrelay.data       import Data


VIEW_LOCATION   = "views"
FILES_LOCATION  = "files"
AGENT_LOCATION  = "agents"


class View():

    def __init__(self, key: str, id: str = None):
        self._agents_repository = Repository(AGENT_LOCATION)
        self._views_repository  = Repository(VIEW_LOCATION)
        self._files_repository  = Repository(FILES_LOCATION)
        self._owner             = Agent(key)
        self._id                = id

        if id:
            self._read()
        else:
            id = Symmetric.key()
            id = Symmetric.id(id)
            self._id = id
            self._write(key, {})


    def _read(self) -> dict:
        with open(f"{self._views_repository}/{self._id}", "r") as file:
            return json.load(file)


    def _write(self, key: str, elements: dict):
        data      = Data(json.dumps(elements))
        signature = data.sign(key)
        content   = {
            "elements":  elements,
            "signer":    self._owner.id,
            "key id":    signature["key id"],
            "signature": signature["signature"]
        }
        with open(f"{self._views_repository}/{self._id}", "w") as file:
            json.dump(content, file)


    @property
    def id(self) -> str:
        return self._id


    @property
    def elements(self) -> dict:
        return self._read()["elements"]


    @property
    def signature(self) -> dict:
        content = self._read()
        return {
            "signer":    content["signer"],
            "key id":    content["key id"],
            "signature": content["signature"]
        }


    def get_element_id(self, element: str) -> str:
        return self._read()["elements"][element]


    def get_element_content(self, element: str) -> bytes:
        id = self.get_element_id(element)
        content = File(self._files_repository)
        content.load(id)
        return content


    def set_element_id(self, key: str, element: str, id: str = None):
        elements = self.elements
        elements[element] = id
        self._write(key, elements)


    def set_element_content(self, key: str, element: str, content: str|bytes = None):
        file = File(self._files_repository, content)
        file.write()
        self.set_element_id(key, element, file.id)


    def verify(self) -> list|bool:
        errors = []

        if not os.path.exists(f"{self._views_repository}/{self.id}"):
            errors.append(f"view '{self.id}': file not found.")

        if not os.path.exists(f"{self._owner.path}"):
            errors.append(f"agent '{self._owner.id}': file not found.")

        for element in self.elements:
            if not os.path.exists(f"{self._files_repository}/{self.elements[element]}"):
                errors.append(f"element '{element}': file not found.")

        self._owner.read()
        key       = str(self._owner.content, "utf-8")
        signature = self.signature

        if self._owner.id != signature["signer"]:
            errors.append(f"signer '{signature['signer']}': conflict ({self._owner.id}).")

        if self._owner.key_id != signature["key id"]:
            errors.append(f"key id '{signature['key id']}': conflict ({self._owner.key_id}).")

        if not Asymmetric.verify(key, bytes(json.dumps(self.elements), "utf-8"), signature["signature"]):
            errors.append(f"signature: invalid signature {signature['signature']}")

        if len(errors) == 0:
            return True
        
        return errors


def test():
    key_pair = Asymmetric.key()
    key      = key_pair["private"]
    view     = View(key)
    print(view.id)
    print(view.elements)
    print(view.signature)

    view = View(key)
    print(view.id)
    print(view.elements)
    print(view.signature)

    view = View(key, view.id)
    print(view.id)
    print(view.elements)
    print(view.signature)

    file = File(Repository(FILES_LOCATION), "content")
    file.write()

    view.set_element_id(key, "element", file.id)
    print(view.id)
    print(view.elements)
    print(view.signature)
    print(view.get_element_id("element"))
    print(view.get_element_content("element"))

    view.set_element_content(key, "new element", "new content")
    print(view.id)
    print(view.elements)
    print(view.signature)
    print(view.get_element_id("element"))
    print(view.get_element_content("element"))
    print(view.get_element_id("new element"))
    print(view.get_element_content("new element"))

    print(view.verify())


__all__ = ["View"]


if __name__ == "__main__":
    test()