"""Data

Definitions for Agent file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/data.py)

## Examples

### Create New Data From Bytes And Get Data Identification

~~~python
from viewrelay import Data

data = Data(b"MY BINARY CONTENT")
id   = data.id
~~~

### Create New Data From String And Get Data Identification

~~~python
from viewrelay import Data

data = Data("MY STRING CONTENT")
id   = data.id
~~~

### Get Binary And String Content Of Data

~~~python
from viewrelay import Data

data = Data("MY STRING CONTENT")

string_content = sr(data)
bytes_content = data.content
~~~

### Encrypt And Decrypt Data Using Symmetric Key

To encrypt data, create or import a key before `Data` initialization.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Symmetric

key               = Symmetric.key()
data              = Data("MY PLAIN CONTENT", key)
id_of_cipher_data = data.id
~~~

If you want encrypt after create data object, use `encrypt` method.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Symmetric

key              = Symmetric.key()
data             = Data("MY PLAIN CONTENT")
plain            = str(data)
id_of_plain_data = data.id

data.encrypt(key)
cipher            = str(data)
id_of_cipher_data = data.id
~~~

To get decrypted value, use `decrypt` method.
The data id not will be changed after decrypt.

~~~python
(...)

data.encrypt(key)

plain             = data.decrypt()
cipher            = str(data)
id_of_cipher_data = data.id
~~~

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A SYMMETRIC KEY.

### Sign And Verify Data Using Asymmetric Key

To sign data, create or import a key before `Data` initialization.
This operation do not make private data content.
To make private data content, use `encrypt` and `decrypt` operations.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Asymmetric

key_pair  = Asymmetric.key()
data      = Data("MY PLAIN CONTENT")
signature = data.sign(key_pair["private"])
~~~

To verify data signature, use `verify` method.

~~~python
(...)

if data.verify(key_pair["public"], signature["signature"]):
    print("OK")
~~~

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A PRIVATE KEY.

Reference
=========
"""
import hashlib
import json
import base58

from viewrelay.encryption import Asymmetric
from viewrelay.encryption import Symmetric

class Data():
    """
    Representation of data to get unique identification.
    If the data content was changed, the identification (`id`) will be changed.
    If data content was encrypted, the `content` of data will be changed.
    After a `decryption` operation applied, the `content` of data not be decrypted, but `content` can be changed.
    """
    
    def __init__(self, content: str|bytes, key: str = None):
        """Constructor of Data

        If `content` parameter is `None`, then `content` will be `b""` (void binary).
        If no `key` defined, the content not will be encrypted.

        > :warning: **WARNING**
        >
        > NEVER PUBLIC SHARE A SYMMETRIC KEY.

        :param content: Content of data.
        :type  content: str|bytes
        :param key:     Optional symmetric key to encrypt data. Default to `None`.
        :type  key:     None|str
        """
        if not content:
            content = ""

        if type(content) is str:
            content = bytes(content, "utf-8")

        self.content = content

        if key:
            self.encrypt(key)


    @property
    def content(self) -> bytes:
        """
        Get binary content of data.

        :return: Content of data.
        :rtype:  bytes
        """
        return self._content


    @content.setter
    def content(self, content: bytes|str):
        """
        Set content of data.

        :param content: Contente of data.
        :type  content: bytes|str
        """
        if type(content) is str:
            content = bytes(content, "utf-8")
        self._content = content


    def encrypt(self, key: str):
        """
        Encrypt data content.

        > :warning: **WARNING**
        >
        > NEVER PUBLIC SHARE A SYMMETRIC KEY.

        :param key: Symmetric key to encrypt data.
        :type  key: None|str
        """
        self._content = Symmetric.encrypt(key, self._content)
        self._content = bytes(self._content, "utf-8")


    def decrypt(self, key: str) -> bytes:
        """
        Get Decrypted data content.

        > :warning: **WARNING**
        >
        > NEVER PUBLIC SHARE A SYMMETRIC KEY.

        :param key: Symmetric key to decrypt data.
        :type  key: None|str

        :return: Decrypted data.
        :rtype: bytes

        """
        return Symmetric.decrypt(key, str(self._content, "utf-8"))


    def sign(self, key: str) -> dict:
        """
        Sign data with asymmetric private key.

        > :warning: **WARNING**
        >
        > NEVER PUBLIC SHARE A PRIVATE ASYMMETRIC KEY.
        
        :param key: Symmetric private key.
        :type  key: str

        :return: signature:
            - `str` **"key id"**:    SHA3-224, encoded in base58, and then, encoded in UTF-8.
            - `str` **"signature"**: RSA private key in DER format, encoded in base58, and then, encoded in UTF-8.
        :rtype: dict
        """
        return Asymmetric.sign(key, self._content)


    def verify(self, key: str, signature: str) -> bool:
        """
        Verify signed data with asymmetric public key.
        If signature correspond to data content, return `True`, else `False`.

        :param key: Symmetric private key.
        :type  key: str

        :return: Result of verification
        :rtype:  bool
        """
        return Asymmetric.verify(key, self._content, signature)


    @property
    def id(self) -> str:
        """
        Unique identification of data based in `content`.

        :return: SHA3-224, encoded in base58, and then, encoded in UTF-8.
        :rtype: str
        """

        buffer = hashlib.sha3_224(self._content)
        buffer = buffer.digest()
        buffer = base58.b58encode(buffer)
        buffer = buffer.decode("utf-8")
        return buffer


    def __str__(self) -> str:
        """
        Get data in string format.

        :return: Data encoded in UTF-8 string.
        :rtype:  str
        """
        return str(self._content, "utf-8")


def test():
    key = Symmetric.key()
    print(key)

    data = Data(b"test")
    print(data.content)
    print(data.id)
    data.encrypt(key)
    print(data.content)
    print(data.id)
    content = data.decrypt(key)
    print(content)
    print(data.id)

    data = Data(b"test", key)
    print(data.content)
    print(data.id)
    content = data.decrypt(key)
    print(content)
    print(data.id)

    data = Data(b"test")
    print(data.content)
    print(data.id)
    key = Asymmetric.key()
    signature = data.sign(key["private"])
    print(signature)
    print(data.verify(key["public"], signature["signature"]))

    data = Data(str("test 2"))
    print(data.content)
    print(data)

    data.content = str("Test 3")
    print(data.content)
    print(data)

    data = Data(None)
    print(data.content)
    print(data)
    print(data.id)
    key = Asymmetric.key()
    signature = data.sign(key["private"])
    print(signature)
    print(data.verify(key["public"], signature["signature"]))
    key = Symmetric.key()
    print(data.encrypt(key))
    print(data.decrypt(key))
    key = Asymmetric.key()
    signature = data.sign(key["private"])
    print(signature)
    print(data.verify(key["public"], signature["signature"]))


__all__ = ["Data"]


if __name__ == "__main__":
    test()
