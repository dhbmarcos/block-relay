import sys
import base58
import json

from Crypto.Random        import get_random_bytes
from Crypto.Cipher        import AES
from Crypto.Cipher        import PKCS1_OAEP
from Crypto.PublicKey.RSA import generate
from Crypto.PublicKey.RSA import import_key
from Crypto.Hash          import SHA3_224
from Crypto.Signature     import pss


ENCRYPTION_SYMMETRIC_KEY_SIZE  = 256
ENCRYPTION_ASYMMETRIC_KEY_SIZE = 4096


class Symmetric():
    def key():
        key_size = int(ENCRYPTION_SYMMETRIC_KEY_SIZE / 8)
        key      = get_random_bytes(key_size)
        key      = base58.b58encode(key)
        key      = str(key, "utf-8")
        return key


    def id(key: str):
        key = base58.b58decode(key)
        id  = SHA3_224.new(key)
        id  = id.digest()
        id  = base58.b58encode(id)
        id  = str(id, "utf-8")
        return id


    def encrypt(key: str, data: bytes) -> str:
        if type(data) is not bytes:
            data = bytes(data, "utf-8")

        key    = base58.b58decode(key)
        cipher = AES.new(key=key, mode=AES.MODE_EAX)
        data   = cipher.encrypt(data)
        data   = base58.b58encode(data)
        data   = data.decode("utf-8")
        nonce  = cipher.nonce
        nonce  = base58.b58encode(nonce)
        nonce  = nonce.decode("utf-8")
        tag    = cipher.digest()
        tag    = base58.b58encode(tag)
        tag    = tag.decode("utf-8")
        data   = [nonce, data, tag]
        data   = json.dumps(data)
        data   = base58.b58encode(data)
        data   = str(data, "utf-8")
        return data


    def decrypt(key: str, data: str) -> bytes:
        key    = base58.b58decode(key)
        data   = base58.b58decode(data)
        data   = data.decode("utf-8")
        data   = json.loads(data)
        nonce  = data[0]
        nonce  = base58.b58decode(nonce)
        tag    = data[2]
        tag    = base58.b58decode(tag)
        data   = data[1]
        data   = base58.b58decode(data)
        cipher = AES.new(key, AES.MODE_EAX, nonce)
        data   = cipher.decrypt(data)
        cipher.verify(tag)
        return data


class Asymmetric():
    def key():
        key_pair = generate(ENCRYPTION_ASYMMETRIC_KEY_SIZE)
        private  = key_pair.export_key("DER")
        private  = base58.b58encode(private)
        private  = str(private, "utf-8")
        public   = key_pair.public_key().export_key("DER")
        id       = SHA3_224.new(public)
        id       = id.digest()
        id       = base58.b58encode(id)
        id       = str(id, "utf-8")
        public   = base58.b58encode(public)
        public   = str(public, "utf-8")
        return {
            "id":      id,
            "public":  public,
            "private": private,
        }


    def id(key: str):
        key = base58.b58decode(key)
        key = import_key(key)

        if key.has_private():
            key = key.public_key().export_key("DER")
        else:
            key = key.export_key("DER")

        id = SHA3_224.new(key)
        id = id.digest()
        id = base58.b58encode(id)
        id = str(id, "utf-8")
        return id


    def encrypt(key: str, data: bytes) -> str:
        key_data = Symmetric.key()
        data     = Symmetric.encrypt(key_data, data)
        key      = base58.b58decode(key)
        key      = import_key(key)
        key_data = base58.b58decode(key_data)
        cipher   = PKCS1_OAEP.new(key)
        key_data = cipher.encrypt(key_data)
        key_data = base58.b58encode(key_data)
        key_data = str(key_data, "utf-8")
        data     = [key_data, data]
        data     = json.dumps(data)
        data     = base58.b58encode(data)
        data     = str(data, "utf-8")
        return data


    def decrypt(key: str, data: str) -> bytes:
        data     = base58.b58decode(data)
        data     = json.loads(data)
        key_data = data[0]
        key      = base58.b58decode(key)
        key      = import_key(key)

        if not key.has_private():
            return None

        cipher   = PKCS1_OAEP.new(key)
        key_data = base58.b58decode(key_data)
        key_data = cipher.decrypt(key_data)
        key_data = base58.b58encode(key_data)
        key_data = str(key_data, "utf-8")
        data     = data[1]
        data     = Symmetric.decrypt(key_data, data)
        return data


    def sign(key: str, data: bytes) -> dict:
        key = base58.b58decode(key)
        key = import_key(key)

        if not key.has_private() or not key.can_sign():
            return False

        hash      = SHA3_224.new(data)
        cipher    = pss.new(key)
        signature = cipher.sign(hash)
        key       = key.export_key("DER")
        key       = base58.b58encode(key)
        key_id    = Asymmetric.id(key)
        signature = base58.b58encode(signature)
        signature = str(signature, "utf-8")
        signature = {
            "key id":    key_id,
            "signature": signature
        }
        return signature


    def verify(key: str, data: bytes, signature: str) -> bool:
        signature = base58.b58decode(signature) 
        key       = base58.b58decode(key)
        key       = import_key(key)
        cipher    = pss.new(key)
        hash      = SHA3_224.new(data)    
        try:
            cipher.verify(hash, signature)
            return True
        except (ValueError):
            return False
