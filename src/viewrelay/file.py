import os

from viewrelay.repository import Repository
from viewrelay.data       import Data
from viewrelay.encryption import Symmetric


class File(Data):
    def __init__(self, repository: Repository, content: str|bytes = None, key: str = None):
        self._repository = repository
        super().__init__(content, key)


    def load(self, id: str):
        path = f"{self._repository}/{id}"

        if not os.path.exists(path):
            raise FileNotFoundError(f"File '{path}' not found.")

        with open(path, "rb") as file:
            self.content = file.read()

        self.verify(id)
        return self


    def verify(self, id):
        if id != self.id:
            raise IOError(f"File '{id}' corrupted.")

        self.content = self._content


    def read(self):
        self.load(self.id)


    def write(self):
        self.content = self._content

        with open(self.path, "wb") as file:
            file.write(self._content)


    @property
    def path(self):
        return f"{self._repository}/{self.id}"


def test():
    repository = Repository("test-fIle")

    file = File(repository)
    file.content = b"Test File"
    file.write()
    id = file.id
    print(file.content)
    print(file.id)
    print(file.path)

    file = File(repository).load(id)
    print(file.content)
    print(file.id)
    print(file.path)

    with open(f"{repository}/{id}", "wb") as f:
        f.write(b"Test File 1")
    
    try:
        file.read()
    except Exception as error:
        print(error)

    print(file.content)
    print(file.id)
    print(file.path)
    file.write()
    
    print(file.path)
    file.read()
    print(file.content)

    id = file.id

    os.remove(f"{repository}/{id}")

    file = File(repository)
    file.content = str("Test File 2")
    file.write()
    print(file.id)
    print(file.path)
    print(file.content)
    print(file)

    key  = Symmetric.key()    
    file = File(repository, "Test File 3", key)
    file.write()
    print(file.id)
    print(file.path)
    print(file.content)
    print(file)


__all__ = ["File"]


if __name__ == "__main__":
    test()