"""Agent

Definitions for Agent file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/agent.py)

## Examples

### Create New Agent

To Create new Agent, need create a RSA key pair first using `Agent.new_key_pair()` static method, and then create new agent.

~~~python
from viewrelay.agent import Agent

key_pair = Agent.new_key_pair()
agent = Agent(key_pair)
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### Import An Agent From Key Pair

To import an Agent from key pair, see example about.

### Import An Agent From Private Key

To import an Agent, need input RSA private key.

~~~python
agent = Agent(YOUR_PRIVATE_KEY_STRING)
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### Import An Agent From Public Key

To import an Agent, need input RSA public key.

~~~python
agent = Agent(YOUR_PUBLIC_KEY_STRING)
~~~

### Load Agent From File

Load from file id:

~~~python
agent = Agent().load("YOUR FILE ID")
~~~

### Check Key Used By Agent

Check `Agent.key_id` is `id` of key pair:

~~~python
(...)

if agent.key_id == key_pair["id"]:
    print("Using this key.")
else:
    print("Using other key.")
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### To Change Default Agent Location

Change `AGENT_LOCATION`:

~~~python
from viewrelay import agent

agent.AGENT_LOCATION = "MY_CUSTOM_PATH_INSIDE_DEFAULT_REPOSITORY"

key_pair = agent.Agent.new_key_pair()
agent = agent.Agent(key_pair)
~~~

> :warning: **WARNING**
>
> Changes in the `AGENT_LOCATION` after create an Agent object, may cause unexpected behavior and data loss.

Reference
=========
"""

import base58

from Crypto.PublicKey.RSA import import_key

from viewrelay.repository import Repository
from viewrelay.file       import File
from viewrelay.encryption import Asymmetric


AGENT_LOCATION = "agents"
"""`str`: Default location of agent repository, in default repository."""

# Issue #4
class Agent(File):
    """User or bot representation throw asymmetric key pair.
    The RSA key pair is need to create an Agent.
    The Agent store public key in agents repository, located in `AGENT_LOCATION`.
    The format of file is a RSA public key in DER format, encoded in base58, and then, encoded in UTF-8.
    """

    @staticmethod
    def new_key_pair() -> dict:
        """Create new key pair.

        > :warning: **WARNING**
        >
        > NEVER PUBLISH A PRIVATE KEY!

        :raise Exception: Failure to create key pair.

        :return: key pair:
            - `str` **"key id"**:  SHA3-224, encoded in base58, and then, encoded in UTF-8.
            - `str` **"private"**: RSA private key in DER format, encoded in base58, and then, encoded in UTF-8.
            - `str` **"public"**:  RSA public key in DER format, encoded in base58, and then, encoded in UTF-8.
        :rtype: dict
        """
        try:
            return Asymmetric.key()
        except Exception as error:
            raise type(error)(f"Failure to create key pair: {str(error)}")


    def __init__(self, key: None|str|dict = None):
        """Constructor of Agent

        If not key input, then a void Agent is created to be loaded from file id using `Agent.load()`.
        The repository is defined in `AGENT_LOCATION`.
        The agents are stored in disk on create.
        if `key` is `dict`, then format is same of key pair dictionary from `Agent.new_key_pair()`.
        if `key` is `str`, then format is RSA key in DER format, encoded in base58, and then, encoded in UTF-8;

        > :warning: **WARNING**
        >
        > NEVER PUBLISH A PRIVATE KEY!

        :param key: Optional key of agent. Default to `None`.
        :type  key: None|str|dict

        :raise ValueError: Key not found to create new agent.
        :raise Exception: Invalid key to create new agent.
        :raise Exception: Failure to create new agent.
        """

        if key == None:
            super().__init__(Repository(AGENT_LOCATION))
        else:
            if type(key) is dict:
                if "public" in key:
                    key = key["public"]
                elif "private" in key:
                    key = key["private"]
                else:
                    raise ValueError("Key not found to create new agent.")

            try:
                key = base58.b58decode(key)
                key = import_key(key)

                if key.has_private():
                    key = key.public_key().export_key("DER")
                else:
                    key = key.export_key("DER")
            except Exception as error:
                raise type(error)(f"Invalid key to create new agent: {str(error)}")

            try:
                key = base58.b58encode(key)
                super().__init__(Repository(AGENT_LOCATION), key)
                self.write()
            except Exception as error:
                raise type(error)(f"Failure to create new agent: {str(error)}")


    @property
    def key_id(self) -> str:
        """Return id of key of agent.
        This information is hash of public key (content of file).
        
        > This information is different of `Agent.id`.

        :raise Exception: Failure to get key id of agent.

        :return: SHA3-224, encoded in base58, and then, encoded in UTF-8.
        :rtype: str
        """

        try:
            return Asymmetric.id(str(self.content, "utf-8"))
        except Exception as error:
            raise type(error)(f"Failure to get key id of agent: {str(error)}")
