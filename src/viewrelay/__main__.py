"""
File relay to implement web3 without centralization in domain name.
    
USAGE:

    python -m viewrelay [--help | --version | --release | --readiness | --build | --checksum]

ARGUMENTS:

    --help:
        Show this help.

    --version:
        Show product version information.

    --release:
        Show product release information.

    --readiness:
        Show product readiness technology level information. See more information at <https://dhbmarcos.gitlab.io/strl/>.

    --build:
        Show product build information.

    --checksum:
        Show source code checksum information.


OPERATIONS:

    Use 'python -m viewrelay --help ' adding operation at end of instruction for more information.

    repository:
        File operations.

    file:
        File operations.

SUPPORT:

    See <https://dhbmarcos.gitlab.io/viewrelay> for more information.

COPYRIGHT:

    Copyright 2024 D. H. B. Marcos

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

CREDITS:

    Deo Ominis Gloria.
"""

import sys
import os

from viewrelay import __version__
from viewrelay import __copyright__
from viewrelay import __release__
from viewrelay import __readiness__
from viewrelay import __checksum__
from viewrelay import __build__

from viewrelay.repository import Repository
from viewrelay.file       import File


DEFAULT_REPOSITORY = "repo"
"""Default repository for CLI operations. Base path is current path."""

DEFAULT_REPOSITORY_FILES = "files"
"""Default files repository for CLI operations."""


def main_spam():
    """Show package name, version and copyright."""
    print(f"{__package__} v{__version__}")
    print(f"{__copyright__}")


def main_help():
    """Show CLI help."""
    main_spam()
    print(__doc__)


def main_version():
    """Show product version information."""
    print(f"{__package__} {__version__}-{__release__}.{__readiness__}+{__build__}.{__checksum__}")


def main_release():
    """Show product release information."""
    print(f"{__package__} {__release__}")


def main_readiness():
    """
    Show product readiness technology level information.
    See more information at <https://dhbmarcos.gitlab.io/strl/>.
    """
    print(f"{__package__} {__readiness__}")


def main_build():
    """
    Show product build information.
    See [viewrelay.\_\_init\_\_.\_\_build\_\_](__init__.md#viewrelay.__init__.__build__") for more information.
    """
    print(f"{__package__} {__build__}")


def main_checksum():
    """Show source code checksum information."""
    print(f"{__package__} {__checksum__}")


def repository_help():
    """
File relay to implement web3 without centralization in domain name.

REPOSITORY OPERATIONS:

    USAGE:

        python -m viewrelay repository [info | sync source]

    ARGUMENTS:

        info:
            Show information of repositories.

        sync source:
            Synchronize two repositories in one way. Source is base of synchronization. Return result of synchronization.

See 'python -m viewrelay --help' for more information.
    """
    main_spam()
    print(repository_help.__doc__)


def repository_info():
    """Show information of repositories."""
    print(f"DEFAULT_REPOSITORY:       {os.getcwd()}/{DEFAULT_REPOSITORY}")
    print(f"DEFAULT_REPOSITORY_FILES: {os.getcwd()}/{DEFAULT_REPOSITORY}/{DEFAULT_REPOSITORY_FILES}")


def repository_sync(source):
    """Synchronize two repositories in one way.

    Source is base of synchronization.
    Return result of synchronization.
    """
    repository = Repository(DEFAULT_REPOSITORY_FILES, DEFAULT_REPOSITORY)
    result     = repository.sync(source)

    for id in result["success"]:
        print(f"File '{id}' received.")

    for failure in result["failure"]:
        print(f"File '{failure['id']}' error: {failure['error']}")

    if len(result["failure"]) > 0:
        exit(5)


def file_help():
    """        
File relay to implement web3 without centralization in domain name.

FILE OPERATIONS:

    USAGE:

        python -m viewrelay file [--help | set | get id]

    ARGUMENTS:

        set:
            Set content of file from standard input. Return identification (id) of file.

        get id:
            Print content of file to standard output using identification (id) of file.

        verify id:
            Verify content of file using identification (id) of file. Return result of verification.

See 'python -m viewrelay --help' for more information.
    """
    main_spam()
    print(file_help.__doc__)


def file_set():
    """Set content of file from standard input.

    Return identification (id) of file.
    """
    repository = Repository(DEFAULT_REPOSITORY_FILES, DEFAULT_REPOSITORY)
    content    = sys.stdin.buffer.read()
    file       = File(str(repository), content)
    file.write()
    print(file.id)


def file_get(id):
    """Print content of file to standard output using identification (id) of file.
    
    :param id: Identification of file. 
    :type  id: str
    """
    repository = Repository(DEFAULT_REPOSITORY_FILES, DEFAULT_REPOSITORY)
    file       = File(str(repository))

    try:
        file = file.load(id)
        file.verify(id)
    except FileNotFoundError:
        print(f"File '{id}' not found.", file=sys.stderr)
        exit(3)
    except IOError as error:
        print(error, file=sys.stderr)
        exit(4)
    
    sys.stdout.buffer.write(file.content)


def file_verify(id):
    """Verify content of file using identification (id) of file.
    
    Return result of verification.
    """
    repository = Repository(DEFAULT_REPOSITORY_FILES, DEFAULT_REPOSITORY)
    file       = File(str(repository))

    try:
        file = file.load(id)
        file.verify(id)
        print(f"File '{id}' OK.")
    except FileNotFoundError:
        print(f"File '{id}' not found.")
        exit(3)
    except IOError as error:
        print(error)
        exit(4)


if __name__ == "__main__":
    arguments = sys.argv[1:]

    try:
        os.getcwd()
    except Exception as failure:
        print(f"Invalid current directory: {failure}", file=sys.stderr)
        exit(1)

    try:
        for argument in arguments:
            if argument in ["--help"]:

                if "file" in arguments:
                    file_help()
                    exit(0)

                if "repository" in arguments:
                    repository_help()
                    exit(0)

                main_help()
                exit(0)

            if argument in ["--version"]:
                main_version()
                exit(0)

            if argument in ["--release"]:
                main_release()
                exit(0)

            if argument in ["--readiness"]:
                main_readiness()
                exit(0)

            if argument in ["--build"]:
                main_build()
                exit(0)

            if argument in ["--checksum"]:
                main_checksum()
                exit(0)

        if len(arguments) > 0 and arguments[0] == "repository":
            if len(arguments) == 2 and arguments[1] == "info":
                repository_info()
                exit(0)

            if len(arguments) == 3 and arguments[1] == "sync":
                source = arguments[2]
                repository_sync(source)
                exit(0)

        if len(arguments) > 0 and arguments[0] == "file":        
            if len(arguments) == 2 and arguments[1] == "set":
                file_set()
                exit(0)

            if len(arguments) == 3 and arguments[1] == "get":
                id = arguments[2]
                exit(file_get(id))

            if len(arguments) == 3 and arguments[1] == "verify":
                id = arguments[2]
                exit(file_verify(id))

        raise Exception(f"Invalid parameters: '{' '.join(arguments)}'.\nRun 'python -m {__package__} --help' argument to more information.")

    except Exception as error:
        print(error, file=sys.stderr)
        exit(2)

