#!/bin/bash -e

echo -n "Package version "

[ ! -v EXIT_SUCCESS ] && readonly EXIT_SUCCESS=0
[ ! -v EXIT_FAILURE ] && readonly EXIT_FAILURE=1

module_installed="$(pip list | grep viewrelay)"
if [ -z "$module_installed" ]
then
    echo -e "\033[01m\033[31mFAILED\033[0m: Module not installed.";
    exit $EXIT_FAILURE;
fi;


read buffer buffer version < <(cat ../../src/viewrelay/__init__.py | grep "__version__");
version="$(echo $version | tr -d '"')"

result="$(venv/bin/python -m viewrelay --version)"
if (($? == 0)) && [[ "$result" == "viewrelay $version"* ]]
then
    echo -e "\033[32mPASSED\033[0m";
else
    echo -e "\033[01m\033[31mFAILED\033[0m: $result";
    exit $EXIT_FAILURE;
fi;
