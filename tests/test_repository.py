import sys
import os

if os.getcwd().split("/")[-1] == "viewrelay":
    sys.path.insert(0, os.path.abspath("src"))
    sys.path.insert(1, os.path.abspath("tests"))

if os.getcwd().split("/")[-1] == "tests":
    sys.path.insert(0, os.path.abspath("../src"))
    sys.path.insert(1, os.path.abspath("../tests"))

from viewrelay.repository import Repository
from viewrelay.repository import REPOSITORY_LOCATION
from viewrelay.file       import File


def test_repository_1():
    repo = Repository("repo-test")
    assert os.path.exists(f"{REPOSITORY_LOCATION}/repo-test")


def test_repository_2():
    repo = Repository("repo-test")
    assert str(repo) == f"{REPOSITORY_LOCATION}/repo-test"


def test_repository_3():
    repo = Repository("repo-test", "/tmp/viewrelay/repo-test-subdir")
    assert os.path.exists("/tmp/viewrelay/repo-test-subdir/repo-test")


def test_repository_4():
    repo = Repository("repo-test", "/tmp/viewrelay/repo-test-subdir")
    assert str(repo) == "/tmp/viewrelay/repo-test-subdir/repo-test"


def test_repository_5():
    repo1  = Repository("repo-test1")
    repo2  = Repository("repo-test2")
    result = repo2.sync(str(repo1))
    assert len(result["success"]) == 0
    assert len(result["failure"]) == 0


def test_repository_6():
    repo1 = Repository("repo-test1")
    file = File(repo1, "0").write()
    file = File(repo1, "1").write()
    file = File(repo1, "2").write()
    file = File(repo1, "3").write()
    file = File(repo1, "4").write()
    file = os.mkdir(f"{REPOSITORY_LOCATION}/repo-test1/dir")
    repo2 = Repository("repo-test2")
    result = repo2.sync(str(repo1))
    assert len(result["success"]) == 5
    assert len(result["failure"]) == 1
