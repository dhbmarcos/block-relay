#!/bin/bash -e

echo -n "Package build "

[ ! -v EXIT_SUCCESS ] && readonly EXIT_SUCCESS=0
[ ! -v EXIT_FAILURE ] && readonly EXIT_FAILURE=1

module_installed="$(pip list | grep viewrelay)"
if [ -z "$module_installed" ]
then
    echo -e "\033[01m\033[31mFAILED\033[0m: Module not installed.";
    exit $EXIT_FAILURE;
fi;


result="$(venv/bin/python -m viewrelay --build)"
if (($? == 0)) && [[ "$result" == "viewrelay "* ]]
then
    echo -e "\033[32mPASSED\033[0m";
else
    echo -e "\033[01m\033[31mFAILED\033[0m: $result";
    exit $EXIT_FAILURE;
fi;
