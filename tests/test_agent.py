import sys
import os

if os.getcwd().split("/")[-1] == "viewrelay":
    sys.path.insert(0, os.path.abspath("src"))
    sys.path.insert(1, os.path.abspath("tests"))

if os.getcwd().split("/")[-1] == "tests":
    sys.path.insert(0, os.path.abspath("../src"))
    sys.path.insert(1, os.path.abspath("../tests"))

import base58
from Crypto.PublicKey.RSA import import_key

from viewrelay.agent      import Agent
from viewrelay.encryption import Asymmetric
from viewrelay.encryption import ENCRYPTION_SYMMETRIC_KEY_SIZE
from viewrelay.encryption import ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_agent_1():
    key = Agent.new_key_pair()
    agent = Agent(key)
    key = import_key(base58.b58decode(agent.content))
    assert not key.has_private()


def test_agent_2():
    key = Agent.new_key_pair()
    agent = Agent(key)
    key = import_key(base58.b58decode(agent.content))
    assert key.size_in_bits() == ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_agent_3():
    key = Agent.new_key_pair()
    agent = Agent(key)
    id = base58.b58decode(agent.id)
    assert len(id) == 224/8


def test_agent_4():
    key = Agent.new_key_pair()
    agent = Agent(key)
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_5():
    key = Agent.new_key_pair()
    del key["public"]
    agent = Agent(key)
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_6():
    key = Agent.new_key_pair()
    del key["private"]
    agent = Agent(key)
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_7():
    key = Agent.new_key_pair()
    key = key["private"]
    agent = Agent(key)
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_8():
    key = Agent.new_key_pair()
    key = key["public"]
    agent = Agent(key)
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_9():
    key = Asymmetric.key()
    agent = Agent(key["private"])
    key = import_key(base58.b58decode(agent.content))
    assert not key.has_private()


def test_agent_10():
    key = Asymmetric.key()
    agent = Agent(key["private"])
    key = import_key(base58.b58decode(agent.content))
    assert key.size_in_bits() == ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_agent_11():
    key = Asymmetric.key()
    agent = Agent(key["private"])
    id = base58.b58decode(agent.id)
    assert len(id) == 224/8


def test_agent_12():
    key = Asymmetric.key()
    agent = Agent(key["private"])
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_13():
    key = Asymmetric.key()
    agent = Agent(key["public"])
    key = import_key(base58.b58decode(agent.content))
    assert not key.has_private()


def test_agent_14():
    key = Asymmetric.key()
    agent = Agent(key["public"])
    key = import_key(base58.b58decode(agent.content))
    assert key.size_in_bits() == ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_agent_15():
    key = Asymmetric.key()
    agent = Agent(key["public"])
    id = base58.b58decode(agent.id)
    assert len(id) == 224/8


def test_agent_16():
    key = Asymmetric.key()
    agent = Agent(key["public"])
    id = base58.b58decode(agent.key_id)
    assert len(id) == 224/8


def test_agent_17():
    assert Agent()


def test_agent_18():
    key_pair = Agent.new_key_pair()
    agent = Agent(key_pair)
    id = agent.id 
    del agent

    agent = Agent().load(id)
    assert agent.id == id


def test_agent_19():
    key_pair = Agent.new_key_pair()
    agent = Agent(key_pair)
    assert agent.id != agent.key_id
