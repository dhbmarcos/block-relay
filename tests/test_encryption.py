import sys
import os

if os.getcwd().split("/")[-1] == "viewrelay":
    sys.path.insert(0, os.path.abspath("src"))
    sys.path.insert(1, os.path.abspath("tests"))

if os.getcwd().split("/")[-1] == "tests":
    sys.path.insert(0, os.path.abspath("../src"))
    sys.path.insert(1, os.path.abspath("../tests"))

import base58

from Crypto.Hash          import SHA3_224
from Crypto.PublicKey.RSA import import_key
from Crypto.Signature     import pss

from viewrelay.encryption import Symmetric
from viewrelay.encryption import Asymmetric
from viewrelay.encryption import ENCRYPTION_SYMMETRIC_KEY_SIZE
from viewrelay.encryption import ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_encryption_symmetric_1():
    key = Symmetric.key()
    assert type(key) is str


def test_encryption_symmetric_2():
    key = Symmetric.key()
    key = bytes(key, "utf-8")
    key = base58.b58decode(key)
    assert len(key) == (ENCRYPTION_SYMMETRIC_KEY_SIZE / 8)


def test_encryption_symmetric_3():
    key = Symmetric.key()
    id  = Symmetric.id(key)
    assert id


def test_encryption_symmetric_4():
    key = Symmetric.key()
    id  = Symmetric.id(key)
    id  = bytes(id, "utf-8")
    id  = base58.b58decode(id)
    assert len(id) == (224 / 8)


def test_encryption_symmetric_5():
    key1 = Symmetric.key()
    key2 = base58.b58decode(key1)
    id   = SHA3_224.new(key2)
    id   = id.digest()
    id   = base58.b58encode(id)
    id   = str(id, "utf-8")
    assert id == Symmetric.id(key1)


def test_encryption_symmetric_6():
    key  = Symmetric.key()
    data = Symmetric.encrypt(key, "test")
    data = Symmetric.decrypt(key, data)
    assert data == b"test"


def test_encryption_asymmetric_1():
    key_pair = Asymmetric.key()
    assert type(key_pair) is dict


def test_encryption_asymmetric_2():
    key_pair = Asymmetric.key()
    assert type(key_pair["public"]) is str


def test_encryption_asymmetric_3():
    key_pair = Asymmetric.key()
    assert type(key_pair["private"]) is str


def test_encryption_asymmetric_4():
    key_pair = Asymmetric.key()
    assert "public" in key_pair


def test_encryption_asymmetric_5():
    key_pair = Asymmetric.key()
    assert "private" in key_pair


def test_encryption_asymmetric_6():
    key_pair = Asymmetric.key()
    key      = key_pair["private"]
    key      = base58.b58decode(key)
    key      = import_key(key)
    assert key.has_private()


def test_encryption_asymmetric_7():
    key_pair = Asymmetric.key()
    key      = key_pair["public"]
    key      = base58.b58decode(key)
    key      = import_key(key)
    assert not key.has_private()


def test_encryption_asymmetric_8():
    key_pair = Asymmetric.key()
    key      = key_pair["private"]
    id       = Asymmetric.id(key)
    assert type(id) is str


def test_encryption_asymmetric_9():
    key_pair = Asymmetric.key()
    key      = key_pair["public"]
    id       = Asymmetric.id(key)
    assert type(id) is str


def test_encryption_asymmetric_10():
    key_pair = Asymmetric.key()
    key      = key_pair["private"]
    id       = Asymmetric.id(key)
    id       = bytes(id, "utf-8")
    id       = base58.b58decode(id)
    assert len(id) == (224 / 8)


def test_encryption_asymmetric_11():
    key_pair = Asymmetric.key()
    key      = key_pair["public"]
    id       = Asymmetric.id(key)
    id       = bytes(id, "utf-8")
    id       = base58.b58decode(id)
    assert len(id) == (224 / 8)


def test_encryption_asymmetric_12():
    key_pair = Asymmetric.key()
    key1     = key_pair["private"]
    key2     = key_pair["private"]
    key2     = base58.b58decode(key2)
    key2     = import_key(key2)
    key2     = key2.public_key().export_key("DER")
    id       = SHA3_224.new(key2)
    id       = id.digest()
    id       = base58.b58encode(id)
    id       = str(id, "utf-8")
    assert Asymmetric.id(key1) == id


def test_encryption_asymmetric_13():
    key_pair = Asymmetric.key()
    key1     = key_pair["private"]
    key2     = key_pair["public"]
    key2     = base58.b58decode(key2)
    key2     = import_key(key2)
    key2     = key2.export_key("DER")
    id       = SHA3_224.new(key2)
    id       = id.digest()
    id       = base58.b58encode(id)
    id       = str(id, "utf-8")
    assert Asymmetric.id(key1) == id


def test_encryption_asymmetric_14():
    key_pair = Asymmetric.key()
    key1     = key_pair["public"]
    key2     = key_pair["private"]
    key2     = base58.b58decode(key2)
    key2     = import_key(key2)
    key2     = key2.public_key().export_key("DER")
    id       = SHA3_224.new(key2)
    id       = id.digest()
    id       = base58.b58encode(id)
    id       = str(id, "utf-8")
    assert Asymmetric.id(key1) == id


def test_encryption_asymmetric_15():
    key_pair = Asymmetric.key()
    key1     = key_pair["public"]
    key2     = key_pair["public"]
    key2     = base58.b58decode(key2)
    key2     = import_key(key2)
    key2     = key2.export_key("DER")
    id       = SHA3_224.new(key2)
    id       = id.digest()
    id       = base58.b58encode(id)
    id       = str(id, "utf-8")
    assert Asymmetric.id(key1) == id


def test_encryption_asymmetric_16():
    key_pair = Asymmetric.key()
    key      = key_pair["private"]
    key      = base58.b58decode(key)
    key      = import_key(key)
    assert key.size_in_bits() == ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_encryption_asymmetric_17():
    key_pair = Asymmetric.key()
    key      = key_pair["public"]
    key      = base58.b58decode(key)
    key      = import_key(key)
    assert key.size_in_bits() == ENCRYPTION_ASYMMETRIC_KEY_SIZE


def test_encryption_asymmetric_18():
    key_pair    = Asymmetric.key()
    key         = key_pair["private"]
    data        = Asymmetric.encrypt(key, "test")
    data        = Asymmetric.decrypt(key, data)
    assert data == b"test"


def test_encryption_asymmetric_19():
    key_pair    = Asymmetric.key()
    key         = key_pair["public"]
    data        = Asymmetric.encrypt(key, "test")
    data        = Asymmetric.decrypt(key, data)
    assert data == None


def test_encryption_asymmetric_20():
    key_pair    = Asymmetric.key()
    key_private = key_pair["private"]
    key_public  = key_pair["public"]
    data        = Asymmetric.encrypt(key_public, "test")
    data        = Asymmetric.decrypt(key_private, data)
    assert data == b"test"


def test_encryption_asymmetric_21():
    key_pair    = Asymmetric.key()
    key_private = key_pair["private"]
    key_public  = key_pair["public"]
    data        = Asymmetric.encrypt(key_private, "test")
    data        = Asymmetric.decrypt(key_public, data)
    assert data == None


def test_encryption_asymmetric_22():
    key_pair  = Asymmetric.key()
    key       = key_pair["public"]
    data      = b"test"
    signature = Asymmetric.sign(key, data)
    assert not signature


def test_encryption_asymmetric_23():
    key_pair  = Asymmetric.key()
    key       = key_pair["private"]
    data      = b"test"
    signature = Asymmetric.sign(key, data)
    assert type(signature) is dict


def test_encryption_asymmetric_24():
    key_pair  = Asymmetric.key()
    key       = key_pair["private"]
    data      = b"test"
    signature = Asymmetric.sign(key, data)
    assert type(signature["key id"]) is str


def test_encryption_asymmetric_25():
    key_pair  = Asymmetric.key()
    key       = key_pair["private"]
    data      = b"test"
    signature = Asymmetric.sign(key, data)
    assert type(signature["signature"]) is str


def test_encryption_asymmetric_26():
    key_pair    = Asymmetric.key()
    key_private = key_pair["private"]
    key_public  = key_pair["public"]
    data        = b"test"
    signature   = Asymmetric.sign(key_private, data)["signature"]
    signature   = base58.b58decode(signature) 
    key_public  = base58.b58decode(key_public)
    key_public  = import_key(key_public)
    cipher      = pss.new(key_public)
    hash        = SHA3_224.new(data)    
    try:
        cipher.verify(hash, signature)
        assert True
    except (ValueError):
        assert False


def test_encryption_asymmetric_27():
    key_pair    = Asymmetric.key()
    key_private = key_pair["private"]
    key_public  = key_pair["public"]
    data        = b"test"
    signature   = Asymmetric.sign(key_private, data)["signature"]
    assert Asymmetric.verify(key_public, data, signature)


def test_encryption_asymmetric_28():
    key_pair    = Asymmetric.key()
    key_private = key_pair["private"]
    key_public  = key_pair["public"]
    data        = b"test"
    signature   = Asymmetric.sign(key_private, data)["signature"]
    assert Asymmetric.verify(key_private, data, signature)
