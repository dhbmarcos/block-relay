import sys
sys.path.append("src")

import base58

import viewrelay


def test_init_1():
    assert viewrelay.__package__ == "viewrelay"


def test_init_2():
    assert str(viewrelay.__copyright__).startswith("Copyright")


def test_init_3():
    assert str(viewrelay.__copyright__).endswith("D. H. B. Marcos")


def test_init_4():
    assert len(viewrelay.__version__) >= 5


def test_init_5():
    assert viewrelay.__release__


def test_init_6():
    assert viewrelay.__readiness__ in ["undefined", "unstable", "stable"]


def test_init_7():
    assert len(viewrelay.__build__) >= 7


def test_init_8():
    assert len(viewrelay.__build__) <= 52


def test_init_9():
    buffer = base58.b58decode(viewrelay.__checksum__)
    length = len(buffer)
    assert length == 28
