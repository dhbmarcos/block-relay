# Table of Contents

* [viewrelay.agent](#viewrelay.agent)
  * [AGENT\_LOCATION](#viewrelay.agent.AGENT_LOCATION)
  * [Agent](#viewrelay.agent.Agent)
    * [new\_key\_pair](#viewrelay.agent.Agent.new_key_pair)
    * [\_\_init\_\_](#viewrelay.agent.Agent.__init__)
    * [key\_id](#viewrelay.agent.Agent.key_id)

<a id="viewrelay.agent"></a>

# viewrelay.agent

Agent

Definitions for Agent file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/agent.py)

## Examples

### Create New Agent

To Create new Agent, need create a RSA key pair first using `Agent.new_key_pair()` static method, and then create new agent.

~~~python
from viewrelay.agent import Agent

key_pair = Agent.new_key_pair()
agent = Agent(key_pair)
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### Import An Agent From Key Pair

To import an Agent from key pair, see example about.

### Import An Agent From Private Key

To import an Agent, need input RSA private key.

~~~python
agent = Agent(YOUR_PRIVATE_KEY_STRING)
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### Import An Agent From Public Key

To import an Agent, need input RSA public key.

~~~python
agent = Agent(YOUR_PUBLIC_KEY_STRING)
~~~

### Load Agent From File

Load from file id:

~~~python
agent = Agent().load("YOUR FILE ID")
~~~

### Check Key Used By Agent

Check `Agent.key_id` is `id` of key pair:

~~~python
(...)

if agent.key_id == key_pair["id"]:
    print("Using this key.")
else:
    print("Using other key.")
~~~

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

### To Change Default Agent Location

Change `AGENT_LOCATION`:

~~~python
from viewrelay import agent

agent.AGENT_LOCATION = "MY_CUSTOM_PATH_INSIDE_DEFAULT_REPOSITORY"

key_pair = agent.Agent.new_key_pair()
agent = agent.Agent(key_pair)
~~~

> :warning: **WARNING**
>
> Changes in the `AGENT_LOCATION` after create an Agent object, may cause unexpected behavior and data loss.

Reference
=========

<a id="viewrelay.agent.AGENT_LOCATION"></a>

#### AGENT\_LOCATION

`str`: Default location of agent repository, in default repository.

<a id="viewrelay.agent.Agent"></a>

## Agent Objects

```python
class Agent(File)
```

User or bot representation throw asymmetric key pair.
The RSA key pair is need to create an Agent.
The Agent store public key in agents repository, located in `AGENT_LOCATION`.
The format of file is a RSA public key in DER format, encoded in base58, and then, encoded in UTF-8.

<a id="viewrelay.agent.Agent.new_key_pair"></a>

#### new\_key\_pair

```python
@staticmethod
def new_key_pair() -> dict
```

Create new key pair.

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

**Raises**:

- `Exception`: Failure to create key pair.

**Returns**:

`dict`: key pair:
- `str` **"key id"**:  SHA3-224, encoded in base58, and then, encoded in UTF-8.
- `str` **"private"**: RSA private key in DER format, encoded in base58, and then, encoded in UTF-8.
- `str` **"public"**:  RSA public key in DER format, encoded in base58, and then, encoded in UTF-8.

<a id="viewrelay.agent.Agent.__init__"></a>

#### \_\_init\_\_

```python
def __init__(key: None | str | dict = None)
```

Constructor of Agent

If not key input, then a void Agent is created to be loaded from file id using `Agent.load()`.
The repository is defined in `AGENT_LOCATION`.
The agents are stored in disk on create.
if `key` is `dict`, then format is same of key pair dictionary from `Agent.new_key_pair()`.
if `key` is `str`, then format is RSA key in DER format, encoded in base58, and then, encoded in UTF-8;

> :warning: **WARNING**
>
> NEVER PUBLISH A PRIVATE KEY!

**Arguments**:

- `key` (`None|str|dict`): Optional key of agent. Default to `None`.

**Raises**:

- `ValueError`: Key not found to create new agent.
- `Exception`: Invalid key to create new agent.
- `Exception`: Failure to create new agent.

<a id="viewrelay.agent.Agent.key_id"></a>

#### key\_id

```python
@property
def key_id() -> str
```

Return id of key of agent.

This information is hash of public key (content of file).

> This information is different of `Agent.id`.

**Raises**:

- `Exception`: Failure to get key id of agent.

**Returns**:

`str`: SHA3-224, encoded in base58, and then, encoded in UTF-8.

