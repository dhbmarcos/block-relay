# Current Main features

- Views based in collection of ontological path;
- Assign ontological paths to files;
- Views signed by agent;
- Agent based in asymmetric keys (RSA 4096 key pair);
- Agent identification based in pubic key;
- Prevent data duplication and check fle corruption using files named by your own hash (SHA3-224).
- Encrypt, decrypt (AES 256 bits and RSA 4096 bits), sign and verify (RSA 4096 bits) data.

