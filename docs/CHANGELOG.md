# Changelog

## [0.3.0]

### Added

- Test of repository;
- Tests improvements;
- Repository synchronization;
- CLI interface for set file;
- CLI interface for get file;
- CLI interface for verify file;
- CLI interface for repository information;
- CLI interface for repository synchronization.

### Changes

- Closes issues #2, #9, #21, #22, #23, $25.

## [0.2.0]

### Added

- Basic CLI interface;
- CLI documentation;
- Initialization documentation;
- Repository documentation;
- Data documentation;
- Add id of symmetric key;
- Test encryption.

### Changes

- Agent documentation;

### Fixes

- Checksum generator.

### Remove

- Config;
- MBSE documentations;
- Stereotypes.

## [0.1.3]

### Added

- Test config;
- Test agent;
- Static method new_key_pair in Agent;
- Help in tools;
- Test of package.

### Changed

- Update package generator;
- Default stereotype now is 'null';
- Test logs;
- Clean tool write in bash.

### Fixes

- Extract generated key pair of Agent (#4);
- Importation path in debug environment.

## [0.0.5]

### Changed

- Fix documentation.

## [0.0.4]

### Changed

- Fix documentation.

## [0.0.3]

### Added

- Encrypt and decrypt operations using AES 256; 
- Encrypt and decrypt operations using RSA 4096; 
- Sign and verify operations using RSA 4096;
- AES 256 key generation;
- RSA 4096 key pair generation;
- Encrypt, decrypt, sign and verify data;
- Hash SHA3-224 of data;
- Repository creator;
- Load, verify, read and write files;
- File path;
- Agent identification based in pubic key;
- Agent based in asymmetric keys;
- Views based in collection of ontological path;
- Views signed by agent;
- Assign ontological paths to files;
- Assign stereotypes to files;
- Get and set views file identifications;
- Get and set views file contents;
- Show all content of view;
- Show signer, signature, raw content and name of view.
- Tool for clean project;
- Tool for update version;
- Debian tool for install;
- Debian tool for test;
- Debian tool for update requirements for virtual environment;
- Debian tool for update virtual environment;
- Debian tool for create and deploy package;
- Documentation page.
