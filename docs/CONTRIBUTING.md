# After clone repository

After clone repository, run `install.sh`.
This script will install packages for debug program.
The script is compatible with Debian.
Tested only in Debian 12.

~~~bash
tools/install.sh
~~~

# How debug

Before debug, enable virtual environment.
Open terminal in project directory and run:

~~~bash
source venv/bin/activate
~~~

You can create a script for debug in root of `src` directory.
Pay attention to remove this script before commit.
Use a code like bellow. 
~~~python
import viewrelay

(...)
~~~


## Codium/VSCode

To run in Codium/VSCode:

1. Press **`CTRL+SHIFT+P`** and type `python: Select Interpreter`;
2. Select `.venv/bin/pyhton3` or type this selecting **Enter interpreter path...** option.

# How to submit changes

## 1. Create a branch

Create a banch of `development` banch and work in this new branch.
Use `fix/` prefix for bug fixes. 
Use `feature/` prefix for new featues. 

## 2. Update local virtual environment
Before commit a change, make sute the local virtual environment is uptated from repository.
The `update-venv.sh` script will update the virtual environmet based in `requirements.txt` file.

~~~bash
tools/update-venv.sh
~~~

## 3. Update remote virtual environment

If you install new Python package, before commit, update `requirements.txt` using `update-requirements.sh` script.

~~~bash
tools/update-requirements.sh
~~~

## 4. Create Merge Request

To submit a change, create a new branch and subimit a merge request.
