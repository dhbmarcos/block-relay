# Table of Contents

* [viewrelay.\_\_main\_\_](#viewrelay.__main__)
  * [DEFAULT\_REPOSITORY](#viewrelay.__main__.DEFAULT_REPOSITORY)
  * [DEFAULT\_REPOSITORY\_FILES](#viewrelay.__main__.DEFAULT_REPOSITORY_FILES)
  * [main\_spam](#viewrelay.__main__.main_spam)
  * [main\_help](#viewrelay.__main__.main_help)
  * [main\_version](#viewrelay.__main__.main_version)
  * [main\_release](#viewrelay.__main__.main_release)
  * [main\_readiness](#viewrelay.__main__.main_readiness)
  * [main\_build](#viewrelay.__main__.main_build)
  * [main\_checksum](#viewrelay.__main__.main_checksum)
  * [repository\_help](#viewrelay.__main__.repository_help)
  * [repository\_info](#viewrelay.__main__.repository_info)
  * [repository\_sync](#viewrelay.__main__.repository_sync)
  * [file\_help](#viewrelay.__main__.file_help)
  * [file\_set](#viewrelay.__main__.file_set)
  * [file\_get](#viewrelay.__main__.file_get)
  * [file\_verify](#viewrelay.__main__.file_verify)

<a id="viewrelay.__main__"></a>

# viewrelay.\_\_main\_\_

File relay to implement web3 without centralization in domain name.

USAGE:

    python -m viewrelay [--help | --version | --release | --readiness | --build | --checksum]

ARGUMENTS:

    --help:
        Show this help.

    --version:
        Show product version information.

    --release:
        Show product release information.

    --readiness:
        Show product readiness technology level information. See more information at <https://dhbmarcos.gitlab.io/strl/>.

    --build:
        Show product build information.

    --checksum:
        Show source code checksum information.


OPERATIONS:

    Use 'python -m viewrelay --help ' adding operation at end of instruction for more information.

    repository:
        File operations.

    file:
        File operations.

SUPPORT:

    See <https://dhbmarcos.gitlab.io/viewrelay> for more information.

COPYRIGHT:

    Copyright 2024 D. H. B. Marcos

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

CREDITS:

    Deo Ominis Gloria.

<a id="viewrelay.__main__.DEFAULT_REPOSITORY"></a>

#### DEFAULT\_REPOSITORY

Default repository for CLI operations. Base path is current path.

<a id="viewrelay.__main__.DEFAULT_REPOSITORY_FILES"></a>

#### DEFAULT\_REPOSITORY\_FILES

Default files repository for CLI operations.

<a id="viewrelay.__main__.main_spam"></a>

#### main\_spam

```python
def main_spam()
```

Show package name, version and copyright.

<a id="viewrelay.__main__.main_help"></a>

#### main\_help

```python
def main_help()
```

Show CLI help.

<a id="viewrelay.__main__.main_version"></a>

#### main\_version

```python
def main_version()
```

Show product version information.

<a id="viewrelay.__main__.main_release"></a>

#### main\_release

```python
def main_release()
```

Show product release information.

<a id="viewrelay.__main__.main_readiness"></a>

#### main\_readiness

```python
def main_readiness()
```

Show product readiness technology level information.
See more information at <https://dhbmarcos.gitlab.io/strl/>.

<a id="viewrelay.__main__.main_build"></a>

#### main\_build

```python
def main_build()
```

Show product build information.
See [viewrelay.\_\_init\_\_.\_\_build\_\_](__init__.md#viewrelay.__init__.__build__") for more information.

<a id="viewrelay.__main__.main_checksum"></a>

#### main\_checksum

```python
def main_checksum()
```

Show source code checksum information.

<a id="viewrelay.__main__.repository_help"></a>

#### repository\_help

```python
def repository_help()
```

File relay to implement web3 without centralization in domain name.

REPOSITORY OPERATIONS:

    USAGE:

        python -m viewrelay repository [info | sync source]

    ARGUMENTS:

        info:
            Show information of repositories.

        sync source:
            Synchronize two repositories in one way. Source is base of synchronization. Return result of synchronization.

See 'python -m viewrelay --help' for more information.

<a id="viewrelay.__main__.repository_info"></a>

#### repository\_info

```python
def repository_info()
```

Show information of repositories.

<a id="viewrelay.__main__.repository_sync"></a>

#### repository\_sync

```python
def repository_sync(source)
```

Synchronize two repositories in one way.

Source is base of synchronization.
Return result of synchronization.

<a id="viewrelay.__main__.file_help"></a>

#### file\_help

```python
def file_help()
```

File relay to implement web3 without centralization in domain name.

FILE OPERATIONS:

    USAGE:

        python -m viewrelay file [--help | set | get id]

    ARGUMENTS:

        set:
            Set content of file from standard input. Return identification (id) of file.

        get id:
            Print content of file to standard output using identification (id) of file.

        verify id:
            Verify content of file using identification (id) of file. Return result of verification.

See 'python -m viewrelay --help' for more information.

<a id="viewrelay.__main__.file_set"></a>

#### file\_set

```python
def file_set()
```

Set content of file from standard input.

Return identification (id) of file.

<a id="viewrelay.__main__.file_get"></a>

#### file\_get

```python
def file_get(id)
```

Print content of file to standard output using identification (id) of file.

**Arguments**:

- `id` (`str`): Identification of file.

<a id="viewrelay.__main__.file_verify"></a>

#### file\_verify

```python
def file_verify(id)
```

Verify content of file using identification (id) of file.

Return result of verification.

