# Version Scheme

The version is composed according SEMVER (https://semver.org).

## Pre-Releases

The **pre-release** is composed by tokens separated by dot (`.`):

- **Release**
- **Readiness** (last token) according Simplest Technology Readiness Level (https://dhbmarcos.gitlab.io/strl)

## Build

Product build instant code.

Specification of build value:
- First five digits:
    - digits 1 and 2: year of build;
    - digits 3 and 4: week number of build;
    - digit  5:       day of week of build;
- separator (`.`);
- Second from start of day;

## Changing

To change the version path, use `tools/update-version`.
This script update version in many files (hardcoded) based in version of `scr/viewrelay/__init__.py`.
Change `scr/viewrelay/__init__.py` file for force a version.
The `tools/update-version` script only update `__version__` and `__build__` in `scr/viewrelay/__init__.py`.
Other parts of version shall be changed in `scr/viewrelay/__init__.py` manually. 
