# Table of Contents

* [viewrelay.data](#viewrelay.data)
  * [Data](#viewrelay.data.Data)
    * [\_\_init\_\_](#viewrelay.data.Data.__init__)
    * [content](#viewrelay.data.Data.content)
    * [content](#viewrelay.data.Data.content)
    * [encrypt](#viewrelay.data.Data.encrypt)
    * [decrypt](#viewrelay.data.Data.decrypt)
    * [sign](#viewrelay.data.Data.sign)
    * [verify](#viewrelay.data.Data.verify)
    * [id](#viewrelay.data.Data.id)
    * [\_\_str\_\_](#viewrelay.data.Data.__str__)

<a id="viewrelay.data"></a>

# viewrelay.data

Data

Definitions for Agent file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/data.py)

## Examples

### Create New Data From Bytes And Get Data Identification

~~~python
from viewrelay import Data

data = Data(b"MY BINARY CONTENT")
id   = data.id
~~~

### Create New Data From String And Get Data Identification

~~~python
from viewrelay import Data

data = Data("MY STRING CONTENT")
id   = data.id
~~~

### Get Binary And String Content Of Data

~~~python
from viewrelay import Data

data = Data("MY STRING CONTENT")

string_content = sr(data)
bytes_content = data.content
~~~

### Encrypt And Decrypt Data Using Symmetric Key

To encrypt data, create or import a key before `Data` initialization.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Symmetric

key               = Symmetric.key()
data              = Data("MY PLAIN CONTENT", key)
id_of_cipher_data = data.id
~~~

If you want encrypt after create data object, use `encrypt` method.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Symmetric

key              = Symmetric.key()
data             = Data("MY PLAIN CONTENT")
plain            = str(data)
id_of_plain_data = data.id

data.encrypt(key)
cipher            = str(data)
id_of_cipher_data = data.id
~~~

To get decrypted value, use `decrypt` method.
The data id not will be changed after decrypt.

~~~python
(...)

data.encrypt(key)

plain             = data.decrypt()
cipher            = str(data)
id_of_cipher_data = data.id
~~~

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A SYMMETRIC KEY.

### Sign And Verify Data Using Asymmetric Key

To sign data, create or import a key before `Data` initialization.
This operation do not make private data content.
To make private data content, use `encrypt` and `decrypt` operations.

~~~python
from viewrelay            import Data
from viewrelay.encryption import Asymmetric

key_pair  = Asymmetric.key()
data      = Data("MY PLAIN CONTENT")
signature = data.sign(key_pair["private"])
~~~

To verify data signature, use `verify` method.

~~~python
(...)

if data.verify(key_pair["public"], signature["signature"]):
    print("OK")
~~~

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A PRIVATE KEY.

Reference
=========

<a id="viewrelay.data.Data"></a>

## Data Objects

```python
class Data()
```

Representation of data to get unique identification.
If the data content was changed, the identification (`id`) will be changed.
If data content was encrypted, the `content` of data will be changed.
After a `decryption` operation applied, the `content` of data not be decrypted, but `content` can be changed.

<a id="viewrelay.data.Data.__init__"></a>

#### \_\_init\_\_

```python
def __init__(content: str | bytes, key: str = None)
```

Constructor of Data

If `content` parameter is `None`, then `content` will be `b""` (void binary).
If no `key` defined, the content not will be encrypted.

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A SYMMETRIC KEY.

**Arguments**:

- `content` (`str|bytes`): Content of data.
- `key` (`None|str`): Optional symmetric key to encrypt data. Default to `None`.

<a id="viewrelay.data.Data.content"></a>

#### content

```python
@property
def content() -> bytes
```

Get binary content of data.

**Returns**:

`bytes`: Content of data.

<a id="viewrelay.data.Data.content"></a>

#### content

```python
@content.setter
def content(content: bytes | str)
```

Set content of data.

**Arguments**:

- `content` (`bytes|str`): Contente of data.

<a id="viewrelay.data.Data.encrypt"></a>

#### encrypt

```python
def encrypt(key: str)
```

Encrypt data content.

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A SYMMETRIC KEY.

**Arguments**:

- `key` (`None|str`): Symmetric key to encrypt data.

<a id="viewrelay.data.Data.decrypt"></a>

#### decrypt

```python
def decrypt(key: str) -> bytes
```

Get Decrypted data content.

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A SYMMETRIC KEY.

**Arguments**:

- `key` (`None|str`): Symmetric key to decrypt data.

**Returns**:

`bytes`: Decrypted data.

<a id="viewrelay.data.Data.sign"></a>

#### sign

```python
def sign(key: str) -> dict
```

Sign data with asymmetric private key.

> :warning: **WARNING**
>
> NEVER PUBLIC SHARE A PRIVATE ASYMMETRIC KEY.

**Arguments**:

- `key` (`str`): Symmetric private key.

**Returns**:

`dict`: signature:
- `str` **"key id"**:    SHA3-224, encoded in base58, and then, encoded in UTF-8.
- `str` **"signature"**: RSA private key in DER format, encoded in base58, and then, encoded in UTF-8.

<a id="viewrelay.data.Data.verify"></a>

#### verify

```python
def verify(key: str, signature: str) -> bool
```

Verify signed data with asymmetric public key.

If signature correspond to data content, return `True`, else `False`.

**Arguments**:

- `key` (`str`): Symmetric private key.

**Returns**:

`bool`: Result of verification

<a id="viewrelay.data.Data.id"></a>

#### id

```python
@property
def id() -> str
```

Unique identification of data based in `content`.

**Returns**:

`str`: SHA3-224, encoded in base58, and then, encoded in UTF-8.

<a id="viewrelay.data.Data.__str__"></a>

#### \_\_str\_\_

```python
def __str__() -> str
```

Get data in string format.

**Returns**:

`str`: Data encoded in UTF-8 string.

