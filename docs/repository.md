# Table of Contents

* [viewrelay.repository](#viewrelay.repository)
  * [REPOSITORY\_LOCATION](#viewrelay.repository.REPOSITORY_LOCATION)
  * [Repository](#viewrelay.repository.Repository)
    * [\_\_init\_\_](#viewrelay.repository.Repository.__init__)
    * [sync](#viewrelay.repository.Repository.sync)
    * [\_\_str\_\_](#viewrelay.repository.Repository.__str__)

<a id="viewrelay.repository"></a>

# viewrelay.repository

Data Repository

Definitions for Repository file.

- [Source Code](https://gitlab.com/dhbmarcos/viewrelay/-/blob/main/src/viewrelay/repository.py)

## Examples

### Create New Repository In Default Location

To create new repository in default location:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME")
~~~

### Create New Repository In Custom Location

To create new repository in custom location:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME", "MY_CUSTOM_LOCATION")
~~~

### To Get Path Of Repository Location

Cast `Repository` to `str`:

~~~python
from viewrelay import repository

repository = repository.Repository("NAME")
path_of_repository = str(repository)
~~~

### To Change Default Repository Location

Change `REPOSITORY_LOCATION`:

~~~python
from viewrelay import repository

repository.REPOSITORY_LOCATION = "MY_DEFAULT_REPOSITORY"

repository = repository.Repository("NAME")
~~~

> :warning: **WARNING**
>
> Changes in the `REPOSITORY_LOCATION` after create an Agent, File, Repository or View object, may cause unexpected behavior and data loss.

### To Synchronize A Repository In One Way.

Use `sync` method:

~~~python
from viewrelay.repository import Repository

repository = Repository("NAME")
result = repository.sync("SOURCE-REPOSITORY-PATH")

print(result["success"])
print(result["failure"])
~~~

Reference
=========

<a id="viewrelay.repository.REPOSITORY_LOCATION"></a>

#### REPOSITORY\_LOCATION

`str`: Default repository location.

<a id="viewrelay.repository.Repository"></a>

## Repository Objects

```python
class Repository()
```

Data repository.

<a id="viewrelay.repository.Repository.__init__"></a>

#### \_\_init\_\_

```python
def __init__(name: str, location: str = REPOSITORY_LOCATION)
```

Create new repository in default repository location.

If not location input, then value of `REPOSITORY_LOCATION` will be used.
If directory already exist do nothing.
The directory will be created at `location`/`name`.

**Arguments**:

- `name` (`str`): Name of directory to be created in default repository location.
- `location` (`str`): Optional custom repository location. Default to `REPOSITORY_LOCATION`.

**Raises**:

- `Exception`: Failure to create repository.

<a id="viewrelay.repository.Repository.sync"></a>

#### sync

```python
def sync(source: str) -> list
```

Synchronize two repositories in one way.

The synchronization is from source to this repository.
On error on copy file from source, the information error will be set in return, in failure list.

**Arguments**:

- `source` (`str`): Path of source repository.

**Raises**:

- `Exception`: Failure in source repositories.

**Returns**:

`dict`: result
- `list` `str`  **"success"**: id of file copied.
- `list` `dict` **"failure"**:
    - `str` **"id"**    : id of file not copied.
    - `str` **"error"** : error information about file copy.

<a id="viewrelay.repository.Repository.__str__"></a>

#### \_\_str\_\_

```python
def __str__()
```

Get path of repository.

