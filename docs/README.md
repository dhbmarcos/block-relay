# Views Relay

File relay to implement web3 without centralization in domain name.

## Main features

- Views based in collection of ontological path;
- Assign ontological paths to files;
- Views signed by agent;
- Agent based in asymmetric keys (RSA 4096 key pair);
- Agent identification based in pubic key;
- Prevent data duplication and check fle corruption using files named by your own hash (SHA3-224).
- Encrypt, decrypt (AES 256 bits and RSA 4096 bits), sign and verify (RSA 4096 bits) data.

## Documentation

* [Full Documentation](https://dhbmarcos.gitlab.io/viewrelay)
* [Changelog](CHANGELOG.md)
* [Contributing](CONTRIBUTING.md)

## Current Status

- Readiness: ![STRL undefined](https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/undefined.svg)
- Version: `0.3.0-public.undefined+24096.20635`
- Checksum: `2tK4NYPbdQdZCXY8Hx29qtgm9cM2og5BLFAf6yZ`


To get checksum, install package and run in a terminal:

~~~bash
python3 -m viewrelay --checksum
~~~

## License

*Copyright 2024 D. H. B. Marcos*

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

**The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.**

***THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.***


## Credits

2024 (C) D. H. B. Marcos.

*Create in 2024.01.09 by D. H. B. Marcos.*

*Deo Ominis Gloria.*
