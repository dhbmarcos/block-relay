# Install

Open a bash terminal and run:

~~~bash
python -m venv venv;
source venv/bin/activate;
pip install -i https://test.pypi.org/simple/ viewrelay;
~~~

To test, run:

~~~bash
python -m viewrelay
~~~

More information, see https://test.pypi.org/project/viewrelay.
