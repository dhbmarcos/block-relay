# Glossary

## AES

The Advanced Encryption Standard (AES), also known by its original name Rijndael is a specification for the encryption of electronic data established by the U.S. National Institute of Standards and Technology (NIST) in 2001.
This systems current use AES 256 bits keys, with EAX mode.
The private keys not stored.

## Agent

User or bot representation throw asymmetric key pair.

See more details in [viewrelay.agent](agent.md).

## Asymmetric Encryption

Encryption scheme using unique shared key.
Allow encrypt and decrypt information sharing a key.
If key is leaked, the security of data is broken.
The system is compatible with AES process.

## Base58

Base58 is a group of binary-to-text encoding schemes used to represent large integers as alphanumeric text.
It is similar to Base64 but has been modified to avoid both non-alphanumeric characters and letters which might look ambiguous when printed.
It is therefore designed for human users who manually enter the data, copying from some visual source, but also allows easy copy and paste because a double-click will usually select the whole string.

## Data

Abstraction of any data.
The content of data is every binary.
The identification of data is a hash content (SHA3-224), encoded in base58, and then, encoded in UTF-8.

Support operations:

- `encrypt`
- `decrypt`
- `sign`
- `verify`

`encrypt` operation use symmetric AES encryption process.
Need the an AES key.
The system not store this key.

`decrypt` operation use symmetric AES encryption process.
Need the an AES key.
The system not store this key.

`sign` operation use asymmetric RSA encryption process.
Need the an RSA private key.
The system not store this key.

`verify` operation use asymmetric RSA encryption process.
Need the an RSA public key.
You can get this key throw an agent.

## Decrypt

Decryption process.
Process to transform the cipher data in plain data.
Can be used RSA (asymmetric) keys and AES (symmetric) keys keys.

## Element

Any part of system or subsystem.
The smallest part of system.

## Encrypt

Encryption process.
Process to transform the plain data in cipher data.
Can be used RSA (asymmetric) keys and AES (symmetric) keys keys.

## File

Operations to store data in a repository.
A specialization of Data class.
Support operations:

- `load`
- `read`
- `write`
- `verify`

`load` operation read data from physical storage file using data identification.

`read` operation update data from physical storage file.

`write` operation update physical storage file.

`verify` check file corruption using data identification.
If physical storage file is modified by another process, the data identification is changed.

## Path

Description of element in according view,

## Private key

Key that do not be published in asymmetric encryption.
Used for encrypt, decrypt and sign operations.

## Public key

Key that can be published in asymmetric encryption.
Used for encrypt and verify operations.

## Repository

Directory for store files according your type.

## RSA

RSA (Rivest–Shamir–Adleman) is a public-key cryptosystem.
This systems current use PKCS#1 OAEP ([RFC8017](https://datatracker.ietf.org/doc/html/rfc8017)) based in RSA, with 4096 bits keys.
The private keys not stored.

## Sign

Signature process.
Process to obtain hash of plain data and cipher hash using private key.
Can be used RSA (asymmetric) keys.

## Signature

Result of sign process.

## Symmetric Encryption

Encryption scheme using key pair (public and private keys).
Allow encrypt, decrypt and sign information without share a key.
If private key is leaked, the security of data is broken.
The system is compatible with RSA process.

## UTF-8

UTF-8 (Unicode Transformation Format – 8-bit) is a variable-length character encoding standard used for electronic communication defined by the Unicode Standard.

## Verify

Verify signature process.
Process to verify signatures using public key.
Can be used RSA (asymmetric) keys.

## View

A point of view of an element in space model.
For example, a car can be seen as a final product, a resource operation, etc.
Futhermore, for example, there are views according an user:

- Final client viewpoint of car;
- Maintenance operator viewpoint of car;
- Engineering viewpoint of car;
- Regulatory agency viewpoint of car.

In this system, is a complex representation of list of path.
Each view store the file identification of content and signature.
The content of view is a list of path, with their respective alias, point to a file.
For change view, is need a private key, for sign the view, by agent.
Can be read the name, content, signer (agent) and signature of view.

Support operations:

- get_element_id;
- get_element_content;
- set_element_id;
- set_element_content;
- verify.
