# Future Features 

## Public Usage

This features will be developed for public data or general public usage: 
        
- Local repositories synchronization
- CLI API
- REST API
- WEB Interface
- File encryption using symmetric keys
- File encryption using asymmetric keys


## Private Usage

This features will be developed for private data or private usage: 

- Relays block list
- Files block list
- Relays pass list
- Files pass list
- Trusted environment