# Summary

## View Relay

- [Introduction](README.md)
- [Current Main Features](features.md)
- [Future Features](future.md)

## Technical Information 

- [Glossary](glossary.md)
- [Install](install.md)
- [Version Scheme](version.md)

## Source Code Reference

- [viewrelay](viewrelay.md)
   - [viewrelay.\_\_init\_\_](__init__.md)
   - [viewrelay.\_\_main\_\_](__main__.md)
   - [viewrelay.agent](agent.md)
   - [viewrelay.data](data.md)
   - [viewrelay.repository](repository.md)

## Other Resources

- [License](LICENSE.md)
- [Contributing](CONTRIBUTING.md)
- [Changelog](CHANGELOG.md)
- [PiPI Page](https://test.pypi.org/project/viewrelay)
- [Source Code](https://gitlab.com/dhbmarcos/viewrelay)
- [Issues](https://gitlab.com/dhbmarcos/viewrelay/-/issues)
