# Table of Contents

* [viewrelay.\_\_init\_\_](#viewrelay.__init__)
  * [\_\_get\_checksum\_\_](#viewrelay.__init__.__get_checksum__)
  * [\_\_version\_\_](#viewrelay.__init__.__version__)
  * [\_\_copyright\_\_](#viewrelay.__init__.__copyright__)
  * [\_\_release\_\_](#viewrelay.__init__.__release__)
  * [\_\_readiness\_\_](#viewrelay.__init__.__readiness__)
  * [\_\_build\_\_](#viewrelay.__init__.__build__)
  * [\_\_checksum\_\_](#viewrelay.__init__.__checksum__)

<a id="viewrelay.__init__"></a>

# viewrelay.\_\_init\_\_

Initialization of View Relay module.

## Example

~~~python
import viewrelay
~~~

Reference
=========

<a id="viewrelay.__init__.__get_checksum__"></a>

#### \_\_get\_checksum\_\_

```python
def __get_checksum__() -> str
```

Calculate hash of source code.

**Returns**:

`str`: SHA3-224, encoded in base58, and then, encoded in UTF-8.

<a id="viewrelay.__init__.__version__"></a>

#### \_\_version\_\_

Version of module (SIMVER)
See <https://simver.org> for more information.

<a id="viewrelay.__init__.__copyright__"></a>

#### \_\_copyright\_\_

Copyright credits of module

<a id="viewrelay.__init__.__release__"></a>

#### \_\_release\_\_

Product release code

<a id="viewrelay.__init__.__readiness__"></a>

#### \_\_readiness\_\_

Simplest Technology Readiness Level.
See <https://dhbmarcos.gitlab.io/strl> for more information.

<a id="viewrelay.__init__.__build__"></a>

#### \_\_build\_\_

Product build instant code.

Specification of build value:
- First five digits:
    - digits 1 and 2: year of build;
    - digits 3 and 4: week number of build;
    - digit  5:       day of week of build;
- separator (`.`);
- Second from start of day;

<a id="viewrelay.__init__.__checksum__"></a>

#### \_\_checksum\_\_

Hash SHA3-224 of source code, generated in run time.

